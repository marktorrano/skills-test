# frontend-api-skills-test

## Project setup
```
npm install
```

### Start server
```
npm run start:api
```

### Compiles and hot-reloads for development
```
npm run serve
```
